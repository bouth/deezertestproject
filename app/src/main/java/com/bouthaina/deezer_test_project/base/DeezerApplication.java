package com.bouthaina.deezer_test_project.base;

import android.app.Activity;
import android.content.Context;

import com.bouthaina.deezer_test_project.di.component.ApplicationComponent;
import com.bouthaina.deezer_test_project.di.component.DaggerApplicationComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

/**
 * Application which handles Services and other Setups before starting the first Activity
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
public class DeezerApplication  extends DaggerApplication {
    private static  Context context;
    private Activity mCurrentActivity = null;
    private static DeezerApplication mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = getApplicationContext();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        ApplicationComponent component = DaggerApplicationComponent.builder().application(this).build();
        component.inject(this);

        return component;
    }

    public static Context getContext() {
        return context;
    }


    public Activity getCurrentActivity() {
        return mCurrentActivity;
    }

    public void setCurrentActivity(Activity mCurrentActivity) {
        this.mCurrentActivity = mCurrentActivity;
    }

    /**
     * Return the application instance.
     * No need to instantiate explicitly!
     *
     * @return
     */
    public static DeezerApplication getInstance() {
        return mInstance;
    }
}
