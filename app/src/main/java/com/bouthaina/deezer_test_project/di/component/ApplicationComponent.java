package com.bouthaina.deezer_test_project.di.component;

import android.app.Application;

import com.bouthaina.deezer_test_project.base.DeezerApplication;
import com.bouthaina.deezer_test_project.di.module.ActivityBindingModule;
import com.bouthaina.deezer_test_project.di.module.ApplicationModule;
import com.bouthaina.deezer_test_project.di.module.ContextModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;


@Singleton
@Component(modules = {ContextModule.class, ApplicationModule.class, AndroidSupportInjectionModule.class, ActivityBindingModule.class})
public interface ApplicationComponent extends AndroidInjector<DaggerApplication> {

    void inject(DeezerApplication application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        ApplicationComponent build();
    }
}