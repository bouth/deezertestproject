package com.bouthaina.deezer_test_project.di.module;

import com.bouthaina.deezer_test_project.ui.main.MainActivity;
import com.bouthaina.deezer_test_project.ui.main.MainFragmentBindingModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract MainActivity bindMainActivity();
}
