package com.bouthaina.deezer_test_project.di.module;


import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.bouthaina.deezer_test_project.di.util.ViewModelKey;
import com.bouthaina.deezer_test_project.ui.deezerlistsongs.DeezerListSongsFragmentViewModel;
import com.bouthaina.deezer_test_project.ui.trackdetails.TrackDetailsFragmentViewModel;
import com.bouthaina.deezer_test_project.util.ViewModelFactory;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


@Singleton
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(DeezerListSongsFragmentViewModel.class)
    abstract ViewModel bindListViewModel(DeezerListSongsFragmentViewModel deezerListSongsFragmentViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(TrackDetailsFragmentViewModel.class)
    abstract ViewModel bindDetailsViewModel(TrackDetailsFragmentViewModel detailsViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
