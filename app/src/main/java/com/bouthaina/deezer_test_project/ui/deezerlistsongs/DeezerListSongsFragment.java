package com.bouthaina.deezer_test_project.ui.deezerlistsongs;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import android.widget.EditText;
import android.widget.TextView;

import com.bouthaina.deezer_test_project.R;
import com.bouthaina.deezer_test_project.base.BaseFragment;
import com.bouthaina.deezer_test_project.data.model.Track;
import com.bouthaina.deezer_test_project.ui.trackdetails.TrackDetailsFragment;
import com.bouthaina.deezer_test_project.ui.trackdetails.TrackDetailsFragmentViewModel;
import com.bouthaina.deezer_test_project.util.ViewModelFactory;
import javax.inject.Inject;
import butterknife.BindView;

public class DeezerListSongsFragment extends BaseFragment implements TrackSelectedListener {

    @BindView(R.id.recyclerView)
    RecyclerView listView;
    @BindView(R.id.tv_error)
    TextView errorTextView;
    @BindView(R.id.loading_view)
    View loadingView;
    @BindView(R.id.txtSearch)
    EditText txtSearch;

    @Inject
    ViewModelFactory viewModelFactory;
    private DeezerListSongsFragmentViewModel viewModel;

    @Override
    protected int layoutRes() {
        return R.layout.screen_list;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DeezerListSongsFragmentViewModel.class);

        listView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        listView.setAdapter(new TrackListAdapter(getContext(), txtSearch, viewModel, this, this));
        listView.setLayoutManager(new LinearLayoutManager(getContext()));

        observableViewModel();
    }

    @Override
    public void onTrackSelected(Track track) {
        TrackDetailsFragmentViewModel detailsViewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(TrackDetailsFragmentViewModel.class);
        detailsViewModel.setSelectedTrack(track);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.screenContainer, new TrackDetailsFragment())
                .addToBackStack(null).commit();
    }


    private void observableViewModel() {

        viewModel.geTrackList().observe(this, track -> {
            if(track != null) listView.setVisibility(View.VISIBLE);
        });

        viewModel.getError().observe(this, isError -> {
            if (isError != null) if(isError) {
                errorTextView.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                errorTextView.setText("An Error Occurred While Loading Data!");
            }else {
                errorTextView.setVisibility(View.GONE);
                errorTextView.setText(null);
            }
        });

        viewModel.getLoading().observe(this, isLoading -> {
            if (isLoading != null) {
                loadingView.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                if (isLoading) {
                    errorTextView.setVisibility(View.GONE);
                    listView.setVisibility(View.GONE);
                }
            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                viewModel.fetchAlbums();
            }
        });
    }
}
