package com.bouthaina.deezer_test_project.ui.trackdetails;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import com.bouthaina.deezer_test_project.data.model.Track;
import com.bouthaina.deezer_test_project.data.rest.Repository;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class TrackDetailsFragmentViewModel extends ViewModel {

    private CompositeDisposable disposable;
    private final MutableLiveData<Track> selectedTrack = new MutableLiveData<>();

    LiveData<Track> getSelectedTrack() {
        return selectedTrack;
    }

    @Inject
    public TrackDetailsFragmentViewModel(Repository repository) {
        disposable = new CompositeDisposable();
    }

    public void setSelectedTrack(Track track) {
        selectedTrack.setValue(track);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}
