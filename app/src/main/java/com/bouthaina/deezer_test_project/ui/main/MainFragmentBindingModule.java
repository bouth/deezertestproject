package com.bouthaina.deezer_test_project.ui.main;


import com.bouthaina.deezer_test_project.ui.deezerlistsongs.DeezerListSongsFragment;
import com.bouthaina.deezer_test_project.ui.trackdetails.TrackDetailsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract DeezerListSongsFragment provideListFragment();

    @ContributesAndroidInjector
    abstract TrackDetailsFragment provideDetailsFragment();
}
