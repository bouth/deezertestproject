package com.bouthaina.deezer_test_project.ui.trackdetails;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bouthaina.deezer_test_project.R;
import com.bouthaina.deezer_test_project.base.BaseFragment;
import com.bouthaina.deezer_test_project.util.ViewModelFactory;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;
import butterknife.BindView;

public class TrackDetailsFragment extends BaseFragment {
    @BindView(R.id.webView1)
    WebView webView;
    @BindView(R.id.btPlay)
    ImageView imageButtonPLay;
    @BindView(R.id.icCover)
    ImageView icCover;
    @BindView(R.id.lblTitle)
    TextView lblTitle;
    @BindView(R.id.artistname)
    TextView artistname;
    @BindView(R.id.webviewLayout)
    LinearLayout webviewLayout;

    @Inject
    ViewModelFactory viewModelFactory;
    private TrackDetailsFragmentViewModel detailsViewModel;

    @Override
    protected int layoutRes() {
        return R.layout.screen_details;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        detailsViewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(TrackDetailsFragmentViewModel.class);
        displayTrackDetails();
        webviewLayout.setOnClickListener(v -> {
            goToWebViewDeezer();
        });

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void  displayTrackDetails(){
        detailsViewModel.getSelectedTrack().observe(this, track -> {
            if (track != null) {
                Animation aniFade = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),R.anim.fade_in);
                Picasso.with(getActivity()).load(track.getArtist().getPictureBig()).into(icCover);
                icCover.startAnimation(aniFade);
                lblTitle.setText(track.getTitle());
                artistname.setText(track.getArtist().getName());

            }
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void goToWebViewDeezer() {
        detailsViewModel.getSelectedTrack().observe(this, track -> {
            if (track != null) {
                webView.getSettings().setJavaScriptEnabled(true);
                webView.loadUrl(track.getLink());
            }
        });
    }
}
