package com.bouthaina.deezer_test_project.ui.deezerlistsongs
import android.app.AlertDialog
import android.app.Dialog
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.DialogInterface
import android.widget.Toast
import com.bouthaina.deezer_test_project.R
import com.bouthaina.deezer_test_project.base.DeezerApplication

import com.bouthaina.deezer_test_project.data.model.TrackList
import com.bouthaina.deezer_test_project.data.rest.Repository

import javax.inject.Inject

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class DeezerListSongsFragmentViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    private var disposable: CompositeDisposable? = null
    private val trackList = MutableLiveData<TrackList>()
    private val repoLoadError = MutableLiveData<Boolean>()
    private val loading = MutableLiveData<Boolean>()

     val error: LiveData<Boolean>
        get() = repoLoadError

    init {
        disposable = CompositeDisposable()
        fetchAlbums()
    }

     fun geTrackList(): LiveData<TrackList> = trackList
     fun getLoading(): LiveData<Boolean> = loading

     fun fetchAlbums() {
        loading.value = true
        disposable!!.add(repository.albums.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : DisposableSingleObserver<TrackList>() {
                    override fun onSuccess(value: TrackList) {
                        if (value.data != null) {
                            repoLoadError.value = false
                            trackList.value = value
                        }else{ showErrorDialog() }
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        repoLoadError.value = true
                        loading.value = false
                        showErrorDialog()
                    }
                }))
    }

    private fun showErrorDialog() {
        AlertDialog.Builder(DeezerApplication.getInstance().currentActivity)
                .setTitle(R.string.error_unknown_title)
                .setMessage(R.string.error_unknown)
                .setNegativeButton(android.R.string.ok, null)
                .setIcon(R.drawable.alert_icon)
                .show()
    }


    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }
}
