package com.bouthaina.deezer_test_project.ui.deezerlistsongs;

import com.bouthaina.deezer_test_project.data.model.Track;

public interface TrackSelectedListener {

    void onTrackSelected(Track track);
}
