package com.bouthaina.deezer_test_project.ui.main;


import android.os.Bundle;

import com.bouthaina.deezer_test_project.R;
import com.bouthaina.deezer_test_project.base.BaseActivity;
import com.bouthaina.deezer_test_project.ui.deezerlistsongs.DeezerListSongsFragment;

import java.util.Observable;

public class MainActivity extends BaseActivity {
    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null)
            getSupportFragmentManager().beginTransaction().add(R.id.screenContainer, new DeezerListSongsFragment()).commit();
    }

    @Override
    public void update(Observable observable, Object o) {
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }
}