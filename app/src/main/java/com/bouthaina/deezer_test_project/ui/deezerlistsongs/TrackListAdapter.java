package com.bouthaina.deezer_test_project.ui.deezerlistsongs;

import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bouthaina.deezer_test_project.R;
import com.bouthaina.deezer_test_project.data.model.Track;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bouthaina.deezer_test_project.base.DeezerApplication.getInstance;

public class TrackListAdapter extends RecyclerView.Adapter<TrackListAdapter.TrackViewHolder>{

    private TrackSelectedListener trackSelectedListener;
    private final List<Track> data = new ArrayList<>();
    private static Context context ;

    TrackListAdapter(Context context ,EditText txtSearch , DeezerListSongsFragmentViewModel viewModel, LifecycleOwner lifecycleOwner, TrackSelectedListener repoSelectedListener) {
        this.trackSelectedListener = repoSelectedListener;
        TrackListAdapter.context = context;
        viewModel.geTrackList().observe(lifecycleOwner, trackList -> {
            data.clear();
            if (trackList != null) {
                data.addAll(trackList.getData());
                filter(txtSearch.getText().toString());
                if(data.size() == 0) {
                    Toast toast=   Toast.makeText(getInstance().getCurrentActivity(), R.string.not_available, Toast.LENGTH_SHORT);
                    toast.setGravity(200,0,300);
                    toast.show();
                }
            }
        });
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public TrackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.track_list_item, parent, false);
        return new TrackViewHolder(view, trackSelectedListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TrackViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }


    private void filter(String charText ) {
        charText = charText.toLowerCase(Locale.getDefault());
        ArrayList<Track> list = new ArrayList<>(data);
        data.clear();
        if (charText.length() == 0) {
            data.addAll(list);
        } else {
            for (Track wp : list) {
                if (wp.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                  data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    static final class TrackViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_track_name)
        TextView repoNameTextView;
        @BindView(R.id.tv_track_description)
        TextView descriptionTextView;
        @BindView(R.id.icCover)
        ImageView image;

         Animation aniFade = AnimationUtils.loadAnimation(context.getApplicationContext(),R.anim.fade_in);

        private Track track;
        TrackViewHolder(View itemView, TrackSelectedListener selectedListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                if(track != null) {
                    selectedListener.onTrackSelected(track);
                }
            });
        }

        void bind(Track track) {
            this.track = track;
            repoNameTextView.setText(this.track.getTitle());
            descriptionTextView.setText(this.track.getArtist().getName());
            Picasso.with(context).load(this.track.getArtist().getPicture()).into(image);
            image.startAnimation(aniFade);
        }
    }
}
