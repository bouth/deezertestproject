package com.bouthaina.deezer_test_project.data.model

/**
 * Deezer model to represent a track list
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
data class TrackList(val data: List<Track>, val total: Int)