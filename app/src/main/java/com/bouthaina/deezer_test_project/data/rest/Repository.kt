package com.bouthaina.deezer_test_project.data.rest

import com.bouthaina.deezer_test_project.data.model.Artist
import com.bouthaina.deezer_test_project.data.model.TrackList
import javax.inject.Inject

import io.reactivex.Single

class Repository @Inject
constructor(private val repoService: TrackService) {

    val artist: Single<Artist>
        get() = repoService.artist

    val albums: Single<TrackList>
        get() = repoService.albums
}