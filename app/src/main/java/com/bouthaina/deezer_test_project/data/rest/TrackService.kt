package com.bouthaina.deezer_test_project.data.rest

import com.bouthaina.deezer_test_project.data.model.Artist
import com.bouthaina.deezer_test_project.data.model.TrackList

import io.reactivex.Single
import retrofit2.http.GET

interface TrackService {

    @get:GET("2.0/user/2529")
    val artist: Single<Artist>


    @get:GET("2.0/user/2529/albums")
    val albums: Single<TrackList>
}
